/*
SQLyog Ultimate v8.61 
MySQL - 5.7.19 : Database - fwc
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`fwc` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `fwc`;

/*Table structure for table `clanes` */

DROP TABLE IF EXISTS `clanes`;

CREATE TABLE `clanes` (
  `id_clan` int(11) NOT NULL AUTO_INCREMENT,
  `path_file` varchar(400) DEFAULT NULL,
  `file_name` varchar(300) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `plataformas` varchar(200) DEFAULT NULL,
  `wins` varchar(200) DEFAULT NULL,
  `edad` varchar(30) DEFAULT NULL,
  `region` varbinary(200) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `activo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_clan`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

/*Data for the table `clanes` */

insert  into `clanes`(`id_clan`,`path_file`,`file_name`,`nombre`,`plataformas`,`wins`,`edad`,`region`,`id_user`,`activo`) values (19,'http://localhost/FortniteWorldClans/assets/img/clanes','tempt.png','INDEX',NULL,'','',NULL,1,'Si'),(20,NULL,'https://image.flaticon.com/icons/png/512/23/23716.png','Sasuke',NULL,'','',NULL,1,'Si'),(21,'http://localhost/FortniteWorldClans/assets/img/clanes','hotel.png','Uchiha',NULL,'','',NULL,1,'Si');

/*Table structure for table `files` */

DROP TABLE IF EXISTS `files`;

CREATE TABLE `files` (
  `id_file` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `path` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_file`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `files` */

insert  into `files`(`id_file`,`name`,`path`,`type`) values (5,'20150115_234042.jpg','C:/laragon/www/FortniteWorldClans/assets/img/clanes/20150115_234042.jpg',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
