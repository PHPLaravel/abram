
$(document).ready(function() {
	try{

		utilerias.initLazy();
		utilerias.initEfectImg1a();
		utilerias.runFunctionsJQ();


	}catch(err){
		console.log("Error js document.ready:");
		console.log(err);
	}

	
});

var urlGral = "http://localhost/FortniteWorldClans/index.php/FortniteClans/";
/*
$('#formulario').submit(function(e){

	e.preventDefault();

	var data = $(this).serialize();
	var x    = $('#url').val();
	var url  = x+'/index.php/FortniteClans/salvarClan'

	//console.log(url);

	
	$.ajax({

		url:url,

		data:data,

		type:'POST',

		success:function(respuesta){
			console.log(respuesta);
		},

		error:function(error){
			console.error(error);
		}

	});
	

});
*/
/*
$('.delete').click(function(e){

	e.preventDefault();

	var id = $(this).attr('data-id');

	$.ajax({

		url:urlGral+'deleteClan/'+id,

		type: 'POST',

		success:function(){
			location.reload();
		}

	});

});
*/




var playerTriton 		= null;
var isValidGeneral;
var scPostMultimediaVideos = null;
var isMobile 			= {	Android 	: function() { return navigator.userAgent.match(/Android/i); },
							BlackBerry 	: function() { return navigator.userAgent.match(/BlackBerry/i); },
							iOS 		: function() { return navigator.userAgent.match(/iPhone|iPad|iPod/i); },
							Opera 		: function() { return navigator.userAgent.match(/Opera Mini/i); },
							Windows 	: function() { return navigator.userAgent.match(/IEMobile/i); },
							any 		: function() { return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows()); } }

utilerias = {
	initLazy : function () {
		try{
			/*$.noConflict();*/
			$('.lazyImg').lazy({
				attribute 			: "data-original",
				scrollDirection 	: 'vertical',
				effect 				: "fadeIn",
				effectTime 			: 2000,
				threshold 			: 0
			});
			$(".effectImg").parent().hover(
				function () {
					try{
						$(this).find(".lazyImg, .swiper-lazy").removeClass("hvr-scale-out");
						$(this).find(".lazyImg, .swiper-lazy").addClass("hvr-scale-in");
					}catch(err){
						console.log("Error js utilerias.initLazy.effect:");
						console.log(err);
					}
				},
				function () {
					try{
						$(this).find(".lazyImg, .swiper-lazy").removeClass("hvr-scale-in");
						$(this).find(".lazyImg, .swiper-lazy").addClass("hvr-scale-out");
					}catch(err){
						console.log("Error js utilerias.initLazy.effect:");
						console.log(err);
					}
				}
			);			

		}catch(err){
			console.log("Error js utilerias.initLazy:");
			console.log(err);
		}	
	},
	showBarFixed : function () {
		try{
			$win 	= $(window);
			var h 	= $("header").height() + 100;
			
			$win.scroll(function () {
				if ($win.scrollTop() > h){
					$(".bMenuFixed").fadeIn(0);
				}
				if ($win.scrollTop() < h) {
					$(".bMenuFixed").fadeOut(0);
				}
			});
		}catch(err){
			console.log("Error js utilerias.showBarFixed:");
			console.log(err);
		}	
	},
	upPage : function () {
		try{
			$("html, body").animate({ scrollTop: 0 }, 500);
		}catch(err){
			console.log("Error js utilerias.upPage:");
			console.log(err);
		}
	},
	overComponent : function (component) {
		try{
			var pathImg = $("#pathImg").val();
			var over 	= $(component).attr("data-over");
			$(component).attr("src", over);
		}catch(err){
			console.log("Error js utilerias.overComponent:");
			console.log(err);
		}
	},
	outComponenet : function (component) {
		try{
			var pathImg = $("#pathImg").val();
			var out 	= $(component).attr("data-out");
			$(component).attr("src", out);
		}catch(err){
			console.log("Error js utilerias.outComponenet:");
			console.log(err);
		}
	}, 
	overOther : function (component) {
		try{
			var other 	= $(component).attr("data-component");
			var over 	= $(component).attr("data-over");
			$(other).attr("src", over);
		}catch(err){
			console.log("Error js utilerias.overOther:");
			console.log(err);
		}
	}, 
	outOther : function (component) {
		try{
			var other 	= $(component).attr("data-component");
			var out 	= $(component).attr("data-out");
			$(other).attr("src", out);
		}catch(err){
			console.log("Error js utilerias.outOther:");
			console.log(err);
		}
	},
	showBars : function (component) {
		try{
			var bar 		= $(component).attr("data-bar").trim();
			var icon 		= $(component).attr("data-icon").trim();

			$("html, body").animate({ scrollTop: 0 }, 500);
			if(!$(bar).is(":visible")){
				$(bar).slideDown(400);
				$(component).find("i").css({
					'-moz-transform'	: 'rotate(360deg)',
					'-webkit-transform'	:' rotate(360deg)',
					'transform' 		:'rotate(360deg)',
					'-moz-transition' 	:' all 0.5s ease',
					'-webkit-transition':' all 0.5s ease',
					'-o-transition'		:' all 0.5s ease',
					'transition'		:' all 0.5s ease'
				}).removeClass(icon).addClass("fa-times");

				$(".ctrlMenus").fadeOut(0);
				$(component).fadeIn(0);
			}else{
				$(".subCategories").fadeOut(0);
				$(".itemsCategories").removeClass("bgs131a");
				
				$(bar).fadeOut(0);
				$(component).find("i").css({
					'-moz-transform'	: 'rotate(0deg)',
					'-webkit-transform'	:' rotate(0deg)',
					'transform' 		:'rotate(0deg)',
					'-moz-transition' 	:' all 0.5s ease',
					'-webkit-transition':' all 0.5s ease',
					'-o-transition'		:' all 0.5s ease',
					'transition'		:' all 0.5s ease'
				}).removeClass("fa-times").addClass(icon);
				$(".ctrlMenus").fadeIn(0);
			}
		}catch(err){
			console.log("Error js utilerias.showBars:");
			console.log(err);
		}
	},
	showSubBars : function (component) {
		try{
			var ID 		= $(component).attr("data-json").trim();

			if(!($(ID).is(":visible"))){
				$(".subCategories").fadeOut(0);
				$(".itemsCategories").removeClass("bgs131a");
				$(component).parents("li").addClass("bgs131a");
				$(ID).fadeIn(200);
			}else{
				$(ID).fadeOut(0);
				$(component).parent("li").removeClass("bgs131a");
			}

		}catch(err){
			console.log("Error js utilerias.showSubBars:");
			console.log(err);
		}
	},
	changePanel : function (component) {
		try{
			var dataTab = $(component).attr("data-tab");
			$(".imageSH").fadeOut(0);
			$(dataTab).fadeIn(1000);
		}catch(err){
			console.log("Error js utilerias.changePanel:");
			console.log(err);
		}
	},
	initEfectImg1a : function(){
		try{
			$(".cAnimation1a").hover(function(){
					$(this).find(".manageImg").removeClass("animation1");
					$(this).find(".manageImg").addClass("animation2");
				}, function(){
					$(this).find(".manageImg").removeClass("animation2");
					$(this).find(".manageImg").addClass("animation1");
			});


			if($(".cAnimation2a").length == 1){	
				var ca2 = setInterval(function(){ 
					try{
						if($(".cAnimation2a").find(".manageImg").attr("data-original") != undefined)
							throw "Aun no carga la imagen";

						clearInterval(ca2);
						$(".cAnimation2a").find(".manageImg").addClass("animation3");

					}catch(err){

					}
				}, 1000);
			}

		}catch(err){
			console.log("Error utilerias.initEfectImg1a: "+err);
		}
	},
	search : function () {
		try{
			var str = $('.iSearch:visible').val().trim();

			if(str == "" || str == undefined || str == null){
				throw "No hay nada que buscar";
			}

			if(str.length <= 2){
				throw "No hay nada que buscar";
			}

			$("#fBusqueda").submit();

		}catch(err){
			console.log("Error js utilerias.search:");
			console.log(err);
		}
	},
	sharePost : function (component) {
		try{
			//utilerias.showIconWhatsApp();
			//utilerias.shareSocial();
			var link 		= $(component).attr("data-link").trim();
			var title 		= $(component).attr("data-title").trim();
			var image 		= $(component).attr("data-img").trim();

			if(link == "" || title == "" || image == "")
				throw "No hay nada que compartir";

			$(".itemSocial").attr("data-link", 		link);
			$(".itemSocial").attr("data-title", 	title);
			$(".itemSocial").attr("data-img", 		image);
			$("#cShareRedes").foundation('open');

		}catch(err){
			console.log("Error js utilerias.sharePost:");
			console.log(err);
		}
	},

	
	shareFB : function (component) {
		try{
			var link 		= $(component).attr("data-link");
			var title 		= $(component).attr("data-title");
			var image 		= $(component).attr("data-img");
			var share 		= encodeURIComponent(link)+"&t="+encodeURIComponent(title)+"&picture="+image;
			share 			= "https://www.facebook.com/sharer/sharer.php?u="+encodeURIComponent(share);
			window.open(share,'Share Facebook','width=626,height=436');
		}catch(err){
			console.log("Error js utilerias.shareFB:");
			console.log(err);
		}
	},
	shareTW : function (component) {
		try{
			var link 		= $(component).attr("data-link");
			var title 		= $(component).attr("data-title");
			var image 		= $(component).attr("data-img");

			if(isMobile.any()){
				var mensaje 			= encodeURIComponent(title) + " - " + encodeURIComponent(link);
				var twitter 			= "twitter://post?message=" + mensaje;
				window.location.href 	= twitter;
			}else{
				var mensaje 			= encodeURIComponent(link)+"&text="+encodeURIComponent(title);
				var twitter 			= "http://twitter.com/intent/tweet?url=" + mensaje;
				window.open(twitter,'Share Twitter','width=626,height=436');
			}
		}catch(err){
			console.log("Error js utilerias.shareTW:");
			console.log(err);
		}
	},
	shareWA : function (component) {
		try{
			var link 		= $(component).attr("data-link");
			var title 		= $(component).attr("data-title");
			var image 		= $(component).attr("data-img");

			var mensaje 	= encodeURIComponent(title) + " - " + encodeURIComponent(link);
			var whatsapp 	= "whatsapp://send?text=" + mensaje;
			window.location = whatsapp;

		}catch(err){
			console.log("Error js utilerias.shareWA:");
			console.log(err);
		}
	},
	showIconWhatsApp : function(){
		try{
			if(isMobile.any()){
				$(".shareWA").fadeIn(0);
			}
			else{
				$(".shareWA").fadeOut(0);
			}

		}catch(err){
			console.log("Error utilerias.showIconWhatsApp: "+err);
		}
	},


	shareSocial : function (component) {
		try{
			var services 			= $(component).attr("alt").trim();
			var link 				= $(component).attr("data-link");
			var title 				= $(component).attr("data-title");
			var image 				= $(component).attr("data-img");

			switch(services){
				case 'facebook':
					// app id 403696616489109
					/*var share 		= encodeURIComponent(link)+"&t="+encodeURIComponent(title)+"&picture="+image;
					share 			= "https://www.facebook.com/sharer/sharer.php?u="+encodeURIComponent(share);*/
					var share = "https://www.facebook.com/dialog/share?app_id=403696616489109&display=popup&href="+encodeURIComponent(link);
					window.open(share,'Share Facebook','width=626,height=436');
					break;
				case 'twitter':
					if(isMobile.any()){
						var mensaje 			= encodeURIComponent(title) + " - " + encodeURIComponent(link);
						var twitter 			= "twitter://post?message=" + mensaje;
						window.location.href 	= twitter;
					}else{
						var mensaje 			= encodeURIComponent(link)+"&text="+encodeURIComponent(title);
						var twitter 			= "http://twitter.com/intent/tweet?url=" + mensaje;
						window.open(twitter,'Share Twitter','width=626,height=436');
					}
					break;
				case 'whatsapp':
					if(isMobile.any()){
						var mensaje 			= encodeURIComponent(title) + " - " + encodeURIComponent(link);
						var whatsapp 			= "whatsapp://send?text=" + mensaje;
						window.location 		= whatsapp;
					}else{
						var mensaje 			= encodeURIComponent(title)+" / "+encodeURIComponent(link);
						var whatsapp 			= "https://api.whatsapp.com/send?text=" + mensaje;
						window.open(whatsapp,'_blank');
					}
					break;
				case 'sms':
					if(isMobile.any()){
						// android 	"sms:/* phone number here */?body=/* body text here */"
						// ios 		"sms:/* phone number here */;body=/* body text here */"
						if( isMobile.Android() ){
							window.location 	= 'sms:'+phone_number+'?body=' + title + " / "+encodeURIComponent(link);;
						}else if(isMobile.iOS()){
							window.location 	= 'sms:'+phone_number+';body=' + title + " / "+encodeURIComponent(link);;
						}
					}
					break;
				case 'line':
					if(isMobile.any()){
						var mensaje 			= encodeURIComponent(title) + " - " + encodeURIComponent(link);
						var line 				= "line://msg/text/?" + mensaje;
						window.location 		= line;
					}else{
						var line 				= 'https://lineit.line.me/share/ui?url='+encodeURIComponent(link)+'&text=' +encodeURIComponent(title);
						window.open(line,'Share Line','width=626,height=436');
					}
					break;
				case 'googleplus':
					var google 					= 'https://plus.google.com/share?url=' +encodeURIComponent(link)+ '&text=' + encodeURIComponent(title);
					window.open(google,'Share Google+','width=626,height=436');
					break;
				case 'blogger':
					var blogger 				= 'https://www.blogger.com/blog-this.g?u=' +encodeURIComponent(link)+ '&n=' + encodeURIComponent(title);
					window.open(blogger,'Share Blogger','width=626,height=436');
					break;
				case 'skype':
					var skype 					= 'https://web.skype.com/share?url=' +encodeURIComponent(link)+ '&text=' + encodeURIComponent(title);
					window.open(skype,'Share Skype','width=626,height=436');
					break;
				case 'telegram':
					if(isMobile.any()){
						var mensaje 			= encodeURIComponent(title) + " - " + encodeURIComponent(link);
						var telegram 			= "tg://msg?text=" + mensaje;
						window.location 		= telegram;
					}else{
						var telegram 			= 'https://t.me/share/url?url=' +encodeURIComponent(link)+ '&text=' + encodeURIComponent(title);
						window.open(telegram,'Share Telegram','width=626,height=436');
					}
					break;
				case 'tumblr':
					var tumblr 					= 'https://www.tumblr.com/widgets/share/tool?canonicalUrl=' + encodeURIComponent(link) + '&title=' + encodeURIComponent(title) + '&caption=&tags=';
					window.open(tumblr,'Share tumblr','width=626,height=436');
					break;
				case 'linkedin':
					var linkedin 				= 'https://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(link) + '&title=' + encodeURIComponent(title) + '&summary=&source=CapitalMéxico';
					window.open(linkedin,'Share linkedin','width=626,height=436');
					break;				
				case 'flipboard':
					var flipboard 				= 'https://share.flipboard.com/bookmarklet/popout?v=2&title=' + encodeURIComponent(title) + '&url=' + encodeURIComponent(link);
					window.open(flipboard,'Share flipboard','width=626,height=436');
					break;
				case 'gmail':
					var contacto 				= $("#formShare #correo").val().trim();
					var gmail 					= 'https://mail.google.com/mail/?view=cm&to=' + contacto + '&su=' + encodeURIComponent(title) + '&body=' + encodeURIComponent(title) +"/ "+encodeURIComponent(link) + '&bcc=&cc=';
					window.open(gmail,'Share flipboard','width=626,height=436');
					break;
				case 'pinterest':
					var pinterest 				= 'http://pinterest.com/pin/create/button/?url=' + encodeURIComponent(link);
					window.open(pinterest,'Share pinterest','width=626,height=436');
					break;
				case 'reddit':
					var reddit 					= 'https://reddit.com/submit?url=' + encodeURIComponent(link) + '&title=' + encodeURIComponent(title);
					window.open(reddit,'Share reddit','width=626,height=436');
					break;
				case 'vk':
					var vk 						= 'http://vk.com/share.php?url=' +  encodeURIComponent(link)  + '&title=' + encodeURIComponent(title) + '&comment=';
					window.open(vk,'Share vk','width=626,height=436');
					break;
				case 'yahoo':
					var contacto 				= $("#formShare #correo").val().trim();
					var yahoo 					= 'http://compose.mail.yahoo.com/?to=' + contacto + '&subject=' + encodeURIComponent(title) + '&body=' + encodeURIComponent(title) +"/ "+encodeURIComponent(link);
					window.open(yahoo,'Share yahoo','width=626,height=436');
					break;
				default: break;
			}
		}catch(err){
			console.log("Error utilerias.shareSocial:");
			console.log(err);
		}
	},
	shareSocialWithData : function (component) {
		try{
			var services 			= $(component).attr("alt").trim();
			var link 				= $(component).attr("data-link");
			var title 				= $(component).attr("data-title");
			var image 				= $(component).attr("data-img");
			var type 				= $(component).attr("data-type");

			$("#btnSuccessShare").attr("alt", 			services);
			$("#btnSuccessShare").attr("data-link", 	link);
			$("#btnSuccessShare").attr("data-title", 	title);
			$("#btnSuccessShare").attr("data-img", 		image);

			switch(services){
				case 'gmail':
				case 'yahoo':
					$("#cShareRedes").foundation('close');
					$("#dataForShare").foundation('open');
					break;
				default: break;
			}

		}catch(err){
			console.log("Error utilerias.shareSocialWithData:");
			console.log(err);
		}
	},
	continueShare : function (component) {
		try{
			isValidGeneral 		= null;
		    $('#formShare').bootstrapValidator('validate');
		    if(isValidGeneral === false)
		    	return false;

		    $("#dataForShare").foundation('close');
		    utilerias.shareSocial(component);

		}catch(err){
			console.log("Error utilerias.continueShare:");
			console.log(err);
		}	
	},

	mobileCheck : function(){
		var check = false;
		try{
			(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
		}catch(err){
			check = false;
			console.log("Error utilerias.mobileCheck: "+err);
		}
		return check;
	},
	enviarCorreo : function(){
		try{
			$(".callout").fadeOut(0);
			$(".cBotonEnviar").fadeOut(0);

			var nombre 	= $("#nombre").val();
			var mail 	= $("#mail").val();
			var asunto 	= $("#asunto").val();
			var mensaje = $("#mensaje").val();

			if(nombre.trim() == "" || mail.trim() == "" || asunto.trim() == "" || mensaje.trim() == ""){
				throw "Verify your information.";
			}

			var data =  { 'action' 	: 'send_mail_page',
						  'nombre' 	: nombre,
						  'mail'	: mail,
						  'asunto'	: asunto,
						  'mensaje'	: mensaje };

			jQuery.post(Contacto.ajaxurl, data, function(response) {
				try{
					console.log(response);
					if(response.success == 2){
						throw response.message;
					}

					$(".mSucces").fadeIn(0);
					$("#nombre").val("");
					$("#mail").val("");
					$("#asunto").val("");
					$("#mensaje").val("");
				}catch(err){
					$(".mAlert .message").html(err);
					$(".mAlert").fadeIn(0);
					console.log("Error utilerias.enviarCorreo.success: "+err);
				}
				$(".cBotonEnviar").fadeIn(0);
			});	
			
		}catch(err){
			console.log("Error utilerias.enviarCorreo: "+err);
			$(".mAlert .message").html(err);
			$(".mAlert").fadeIn(0);
			$(".cBotonEnviar").fadeIn(0);
			
		}
	},
	runFunctionsJQ : function () {
		try{
			
			var swiper = new Swiper('.swiperhome', {
    		  navigation: {    		    
    		    effect: 'slide',
    		  },
    		  autoplay: {
		    	delay: 5000,
			  },	
    		});
    					
		}catch(err){
			console.log("Error js utilerias.runFunctionsJQ:");
			console.log(err);
		}
	},
	initSwiperVideos : function () {
		try{
			if(scPostMultimediaVideos != null)
				throw "Ya se creo el slide";

			if($(document).find(".scPostMultimediaVideos").length > 0){
				scPostMultimediaVideos = new Swiper('.scPostMultimediaVideos', {
			      	slidesPerView 		: 1, // columnas
			      	spaceBetween 		: 0,
			      	preloadImages 		: true,
					lazyLoading 		: true,
			      	pagination: {
				        el 				: '.spVideos',
				        type 			: 'fraction',
				     },
			      	navigation: {
				        nextEl : '.sbnPostMultimediaVideos',
				        prevEl : '.sbpPostMultimediaVideos',
				    }
			    });
			}
		}catch(err){
			console.log("Error js utilerias.runFunctionsJQ:");
			console.log(err);
		}
	},
	changeFotoGallery : function (component) {
		try{
			var bro 	= $(component).siblings('.swiper-slide');
			var total 	= bro.length;

			if(total == 0)
				throw "No hay nada que hacer";

			if($(component).hasClass("active-bar"))
				throw "No hay nada que hacer";

			var img 	= $(component).attr("data-image-guid").trim();

			if(img == "")
				throw "No hay foto que mostrar";

			$("#de-total").html(($(component).index() + 1));
			$("#cImageShow").attr("style", "background-image: url('"+img+"');");

			for(var i = 0; i < total; i++)
				$(bro[i]).removeClass("active-bar").find(".cmask").addClass("bgt108a");;			

			$(component).addClass("active-bar").find(".cmask").removeClass("bgt108a");

		}catch(err){
			console.log("Error js utilerias.changeFotoGallery:");
			console.log(err);
		}	
	},
	selectDateHoroscopos : function () {
		try{
			jQuery.datetimepicker.setLocale('es');
			jQuery('#selectFecha').datetimepicker({
				format 		: 'Y-m-d',
				inline  	: true,
				lang 		: 'es',
				timepicker  : false,
				startDate 	: new Date(),
				maxDate 	: 0,
				onSelectDate:function(currentTime, $i){
					try{
						var newDate = moment(new Date(currentTime)).format("YYYY-MM-DD");
						var pos 	= $( ("."+newDate) ).index();

						if(pos == -1)
							throw "";

						var mySwiper = document.querySelector('.scCategoryHoroscopos').swiper;
						mySwiper.slideTo((pos-1));
						$("#txtFecha").html("Fecha "+moment(new Date(currentTime)).locale('es').format("DD MMMM YYYY"));

					}catch(err){
						console.log("ERROR js utileriasMultimedia.createCalendarCopy.onSelectDate:");
						console.log(err);
					}
				}
			});

		}catch(err){
			console.log("Error js utilerias.selectDateHoroscopos:");
			console.log(err);
		}	
	},
	runWebservice : function () {
		try{
			if($(document).find(".cWS").length == 0)
				throw "No hay contenedores para colocar los servicios";

			var subdominio 	= $("#subdominio").val().trim();
			if(subdominio == "")
				throw "No hay subdominio";

			var efekto 		= {
				"name" 			: "efekto",
				"component" 	: ".cEfektoTV",
				"componentP" 	: ".cEfektoTVPost",
				"storage" 		: "dataefekto",
				"saved" 		: "",
				"dominio" 		: "https://www.efekto.tv/",
				"webservice" 	: "https://www.efekto.tv/webservices/webservices.php",
				"config" 		: {
					"trigger" 	: 0,
					"firts" 	: 1,
					"bloques"   : encodeURIComponent(JSON.stringify([
						{
							"name" 				: "bloque1",
							"importancia" 		: [/*
								{
									"key"  	: "importancia_post",
									"value" : "Normal"
								},
								{
									"key"  	: "importancia_post",
									"value" : "Destacada"
								},
								{
									"key"  	: "importancia_post",
									"value" : "Principal"
								}*/],
							"categorias" 		: ["ntt", "videos", "entrevistas"],
							"query" 			: 1,
							"limite" 			: 6,
							"videoKey" 			: "videos_kaltura_id-s",
							"audioKey" 			: "audios_text"
						}
					]))
				}	
			}

			var thenews 		= {
				"name" 			: "thenews",
				"component" 	: ".cTheNews",
				"componentP" 	: ".cTheNewsPost",
				"storage" 		: "datathenews",
				"saved" 		: "",
				"dominio" 		: "https://www.thenews.mx/",
				"webservice" 	: "https://www.thenews.mx/webservices/webservices.php",
				"config" 		: {
					"trigger" 	: 0,
					"firts" 	: 1,
					"bloques"   : encodeURIComponent(JSON.stringify([
						{
							"name" 				: "bloque1",
							"importancia" 		: [],
							"categorias" 		: ["business", "entertainment", "latest-news", "living", "mexico", "science", "sports", "world"],
							"query" 			: 1,
							"limite" 			: 4,
							"videoKey" 			: "",
							"audioKey" 			: ""
						}
					]))
				}	
			}

			var cambio 		= {
				"name" 			: "cambio",
				"component" 	: ".cCambio",
				"componentP" 	: ".cCambioTVPost",
				"storage" 		: "datacambio",
				"saved" 		: "",
				"dominio" 		: "https://www.revistacambio.com.mx/",
				"webservice" 	: "https://www.revistacambio.com.mx/webservices/webservices.php",
				"config" 		: {
					"trigger" 	: 0,
					"firts" 	: 1,
					"bloques"   : encodeURIComponent(JSON.stringify([
						{
							"name" 				: "bloque1",
							"importancia" 		: [],
							"categorias" 		: ["cultura", "deportes", "dinero", "mundo", "tendencias", "panorama", "tecno", "salud", "entretenimiento"],
							"query" 			: 1,
							"limite" 			: 4,
							"videoKey" 			: "",
							"audioKey" 			: ""
						}
					]))
				}	
			}

			var gaming 		= {
				"name" 			: "gaming",
				"component" 	: ".cCapitalGaming",
				"componentP" 	: ".cCapitalGamingPost",
				"storage" 		: "datacapitalgaming",
				"saved" 		: "",
				"dominio" 		: "https://capitalgaming.mx",
				"webservice" 	: "https://capitalgaming.mx/webservices/webservices.php",
				"config" 		: {
					"trigger" 	: 0,
					"firts" 	: 1,
					"bloques"   : encodeURIComponent(JSON.stringify([
						{
							"name" 				: "bloque1",
							"importancia" 		: [],
							"categorias" 		: ["destacado", "eventos", "ligas", "movil", "nintendo", "noticias", "pc", "ps", "xbox"],
							"query" 			: 1,
							"limite" 			: 4,
							"videoKey" 			: "",
							"audioKey" 			: ""
						}
					]))
				}	
			}

			efekto  		= utilerias.checkStorage(efekto);
			utilerias.getPostsService(efekto);

			thenews  		= utilerias.checkStorage(thenews);
			utilerias.getPostsService(thenews);

			cambio  		= utilerias.checkStorage(cambio);
		 	utilerias.getPostsService(cambio);

			gaming  		= utilerias.checkStorage(gaming);
			utilerias.getPostsService(gaming);

		}catch(err){
			console.log("Error utilerias.runWebservice:");
			console.log(err);
		}
	},
	getPostsService : function (config) {
		try{
			$.ajax({
		    	type 			: "GET",
		        url 			: config.webservice,
		        data 			: config.config,
	        	crossDomain 	: true,
	   			beforeSend 		: function(){  },
		        success 		: function (success) {
		        	try{
		        		var posts 					= "";
		        		if(parseInt(success.success) == 1){ // nuevos post
		        			var a 					= {
								"trigger" 	: success.trigger,
								"data" 		: success.posts
							}
		        			localStorage.setItem(config.storage, encodeURIComponent(JSON.stringify(a)));

		        			posts  					= success.posts;
		        		}else{
		        			if(config.saved != "") // obten los viejos post
		        				posts 				= config.saved;	
		        		}

		        		switch(config.name) {
						    case "efekto" 	: utilerias.writeEfekto(posts, config); 	break;
						    case "thenews" 	: utilerias.writeTheNews(posts, config);	break;
						    case "cambio" 	: utilerias.writeCambio(posts, config);		break;
						    case "gaming" 	: utilerias.writeGaming(posts, config);		break;
						    default 		: break;
						}

		        	}catch(err){
		        		console.log("Error utilerias.getPostsService.ajax.success:");
						console.log(err);
		        	}
		        },
		        error(xhr,status,error){
		        	console.log("ERROR JS utilerias.getPostsService.ajax.error: ");
		        	console.log(xhr.responseText);
		        }
		    });

		}catch(err){
			console.log("Error utilerias.getPostsService:");
			console.log(err);
		}
	},
	checkStorage : function (config) {
		try{
			if (typeof(Storage) === "undefined")
			    throw "No esta activo localstorage para este navegador";

			var trigger 				= 0;
			var data 					= [];

			var getItem  				= localStorage.getItem(config.storage)
			if(getItem == null){
				var a 					= {
					"trigger" 	: trigger,
					"data" 		: data
				}
				localStorage.setItem(config.storage, encodeURIComponent(JSON.stringify(a)));
				config.config.firts 	= 1;
			}else{
				getItem 				= JSON.parse(decodeURIComponent(getItem));
				config.saved 			= getItem.data;
				config.config.trigger 	= getItem.trigger;
				config.config.firts 	= 0;
			}
		}catch(err){
			console.log("Error utilerias.checkStorage:");
			console.log(err);
		}
		return config;
	},
	writeEfekto : function (posts, config) {
		try{
			var bloque 			= posts.bloque1;

			if(!$.isArray(bloque))
				throw "no hay notas para efekto";

			var componentP 		= config.componentP;
			var components 		= $(componentP).find(".cSinglePost");

			if(components.length == 0)
				throw "no hay contenedores";				

			var total 			= bloque.length;
			var items 			= "";

			for(var i = 0; i < total; i++){
				var id 			= bloque[i].ID;
				var time 		= bloque[i].formatTime;
				var guid 		= bloque[i].guid;
				var formatLink 	= bloque[i].formatLink;
				var image 		= bloque[i].image;
				var imageMedium	= image.medium;
				var imageFull	= image.full;
				var imageAlt	= image.title;
				var postExcerpt = bloque[i].post_excerpt;
				var postTitle 	= bloque[i].post_title;
				var videos 		= bloque[i].videos;

				var l 			= 35;
				if(i == 0)
					l 			= 60;

				$(components[i]).find(".txtTitle").attr("title", postTitle);
				$(components[i]).find(".txtTitle").attr("alt", postTitle);
				$(components[i]).find(".txtTitle").attr("href", formatLink);
				$(components[i]).find(".txtTitle h3").html(utilerias.lc(postTitle, l));
				$(components[i]).find(".txtTime").html(time);
				$(components[i]).find(".manageImg").attr("alt", imageAlt);
				$(components[i]).find(".manageImg").attr("title", imageAlt);
				$(components[i]).find(".manageImg").attr("data-original", imageMedium);

				if(videos.trim() == "")
					$(components[i]).find(".iVideo").fadeOut(0);

				$(components[i]).find(".iVideo").attr("data-json", encodeURIComponent(JSON.stringify(videos)));

				$(components[i]).find(".fa-share-alt").attr("data-title", postTitle);
				$(components[i]).find(".fa-share-alt").attr("data-img", imageFull);
				$(components[i]).find(".fa-share-alt").attr("data-link", formatLink);

			}// fin for

			if(total > 0)
				$(config.component).fadeIn(0);


		}catch(err){
			console.log("Error utilerias.writeEfekto:");
			console.log(err);
			$(config.component).fadeOut(0);
		}
	},
	playVideo : function (component) {
		try{
			var dataJson 		= $(component).attr("data-json").trim();

			if(dataJson == "")
				throw "No hay datos de reproducción";

			dataJson 			= JSON.parse(decodeURIComponent(dataJson));
			dataJson 			= dataJson.split(",");

			if(dataJson.length == 0)
				throw "No hay datos de reproducción";

			$(".swcPlayerVideo .swiper-wrapper").html("");

			var players 		= "";
			for(var i = 0 in dataJson){
				var entryID 	= dataJson[i];

				if(entryID.trim() == "")
					continue;

				var id 			= ("video"+entryID);
				players 		+= 	'<div class="swiper-slide position-relative H100 overflow-hidden swiper-no-swiping">'+
										'<div class="cVideo W100 H100 playersKaltura '+id+'" id="'+id+'"></div>'+
									'</div>';

			}// fin for

			if(players.trim() == "")
				throw "No hay nada que reproducir";

			$(".swcPlayerVideo .swiper-wrapper").html(players);

			var prerolURL = "//v.lkqd.net/ad?pid=410&sid=350296&output=vastvpaid&support=html5flash&execution=instream&placement=preroll&playinit=user&volume=75&width=[WIDTH]&height=[HEIGHT]&dnt=[DO_NOT_TRACK]&pageurl=[PAGEURL]&contentid=[CONTENT_ID]&contenttitle=[CONTENT_TITLE]&contentlength=[CONTENT_LENGTH]&contenturl=[CONTENT_URL]&rnd=[CACHEBUSTER]";
			if(utilerias.mobileCheck())
				prerolURL = "//v.lkqd.net/ad?pid=410&sid=350298&output=vastvpaid&support=html5&execution=instream&placement=preroll&playinit=user&volume=75&width=[WIDTH]&height=[HEIGHT]&dnt=[DO_NOT_TRACK]&pageurl=[PAGEURL]&contentid=[CONTENT_ID]&contenttitle=[CONTENT_TITLE]&contentlength=[CONTENT_LENGTH]&contenturl=[CONTENT_URL]&rnd=[CACHEBUSTER]";


			var t;
	        var r       	= false;
	        var s 			= null;

	        s 				= document.createElement('script');
	        s.type      	= 'text/javascript';
	        s.async     	= '';
	        s.defer 		= '';
	        s.src       	= 'https://cdnapisec.kaltura.com/p/1658161/sp/165816100/embedIframeJs/uiconf_id/27391242/partner_id/1658161';

	        s.onload 		= 	s.onreadystatechange = function() {
	                            	/*console.log( this.readyState );*/
	                            	if ( !r && (!this.readyState || this.readyState == 'complete') ){
	                                	r 					= true;

	                                	for(var i = 0 in dataJson){
	                                		var entryID 	= dataJson[i];
											var id 			= ("video"+entryID);

	                                		kWidget.embed({
												'targetId' 	: 	id,
												'wid' 		: 	'_1658161',
												'uiconf_id' : 	'27391242',
												'entry_id' 	: 	entryID,
												'flashvars'	: 	{ 	
													"streamerType" 	: "auto",
													'autoPlay' 		: false,
													/*'myComponent'	: { 
														'plugin' 		: true,
														'iframeHTML5Css':'http://www.efekto.tv/wp-content/themes/EfecktoTV/style.css' 
													},*/
													"comscore"		: {	
														"plugin"	 		: true,
														"position" 			: "before",
														"cTagsMap" 			: "//projects.kaltura.com/ran/comscore/comscore.xml", 
														"trackEventMonitor" : "trackEvent",
														"c2" 				: "19249540",
														"asyncInit" 		: true,
														"relativeTo" 		: "video"
													},
													"vast": {
														"prerollUrl" 		: prerolURL,
														"numPreroll" 		: "1",
														"prerollStartWith" 	: "1",
														"prerollInterval" 	: "1",
														"preSequence" 		: "1",
														"numPostroll" 		: "1",
														"postSequence" 		: "1",
														"overlayUrl" 		: "//projects.kaltura.com/MichalR/vast_overlay.xml",
														"overlayStartAt" 	: "20",
														"overlayInterval" 	: "300",
														"timeout" 			: "4"
													},
													"adsOnReplay" : true,
													"inlineScript" : false
												},
												'params'	: 	{ 'wmode' : 'transparent' }
											});	
	                                	}
	                                	

										$("#cPlayerVideo").foundation('open');

										var swiper = new Swiper('.swcPlayerVideo', {
									      	pagination : {
									        	el 		: '.swiper-pagination',
									        	type 	: 'progressbar',
									      	},
									      	navigation : {
									        	nextEl 	: '.swNextPlayerVideo',
									        	prevEl 	: '.swPrevPlayerVideo',
									      	},
									      	noSwiping 	: true,

									    });	

									    kWidget.addReadyCallback( function( playerId ){
											$('.swNextPlayerVideo, .swPrevPlayerVideo').click(function(){
												var kdp = document.getElementsByClassName("playersKaltura");
												for(var i = 0 in kdp){
													kdp[i].sendNotification( "doPause" );
												}
											});
										});

									    $("#cPlayerVideo").off();
										$("#cPlayerVideo").on('closed.zf.reveal', function () {
					                   	 	try{
					                   	 		$("#cPlayerVideo #cScriptVideoPlayer").html("");
					                   	 		$("#cPlayerVideo .swcPlayerVideo .swiper-wrapper").html("");
					                   	 	}catch(err){
					                   	 		console.log("Error utilerias.playVideo.closed.zf.reveal:");
												console.log(err);
					                   	 	}
					                	});	

	                            	}
	                        	};

	        t           	= document.getElementById("cScriptVideoPlayer");
	        t.appendChild(s); 

		}catch(err){
			console.log("Error utilerias.playVideo:");
			console.log(err);
		}
	},
	playVideoJWP : function (component) {
		try{
			var dataJson 		= $(component).attr("data-json");
			var dataTitle 		= $(component).attr("data-title");

			if(dataJson == undefined)
				throw "No hay datos de reproducción";

			dataJson 			= dataJson.trim();

			if(dataJson == "")
				throw "No hay datos de reproducción";

			dataJson 			= dataJson.split(",");

			if(dataJson.length == 0)
				throw "No hay datos de reproducción";

			if(dataTitle == undefined)
				dataTitle 		= " - ";

			if(dataTitle == "")
				dataTitle 		= " - ";

			$("#cPlayerVideoJWP .swcPlayerVideoJWP .swiper-wrapper").html("");
			$("#cPlayerVideoJWP #txtTitle").html(dataTitle);

			var players 		= "";
			for(var i = 0 in dataJson){
				var mediaID 	= dataJson[i];

				if(mediaID.trim() == "")
					continue;

				var ID 			= "jwp"+mediaID;

				players 		= 	'<div class="swiper-slide position-relative H100 overflow-hidden swiper-no-swiping"><div id="'+ID+'"></div></div>';
				
				$("#cPlayerVideoJWP .swcPlayerVideoJWP .swiper-wrapper").append(players);

				jwplayer((ID.toString())).setup({ 
			        "playlist": "https://cdn.jwplayer.com/v2/media/"+mediaID
			    });

			}// fin for

			//$("#cPlayerVideoJWP").off();
			$("#cPlayerVideoJWP").foundation('open');

			var swiper = new Swiper('.swcPlayerVideoJWP', {
			  	pagination : {
			    	el 					: '.swpJWP',
			    	clickable 			: true,
			       	dynamicBullets 		: true,
			       	renderBullet 		: function (index, className) {
			         	return '<span style="line-height: normal;" class="WA HA PTB10 PLR15 rad104a fSize16 col101a bgs131a Roboto-BoldCondensed ' + className + '">' + (index + 1 ) + '</span>';
			       	}
			  	},
			  	navigation : {
			    	nextEl 			: '.swNextPlayerVideoJWP',
			    	prevEl 			: '.swPrevPlayerVideoJWP',
			    	disabledClass	: 'swiper-button-disabled'
			  	},
			  	allowTouchMove 		: false,
			  	noSwiping 			: true,
			  	autoHeight 			: true
			});	


			$('.swNextPlayerVideoJWP, .swPrevPlayerVideoJWP').click(function(){
				try{
					var cSlides = $("#cPlayerVideoJWP .swcPlayerVideoJWP .swiper-wrapper .swiper-slide");
					var total 	= cSlides.length;

					if(total == 0)
						throw "No hay videos";

					for(var i = 0; i < total; i++)
						jwplayer(i).pause();
					
				}catch(err){
					console.log("Error utilerias.playVideoJWP:");
					console.log(err);
				}
			});

			$("#cPlayerVideoJWP").on('closed.zf.reveal', function () {
			 	try{
			 		swiper.destroy(true, true);
			 		$("#cPlayerVideoJWP .swcPlayerVideoJWP .swiper-wrapper").html("");
			 	}catch(err){
			 		console.log("Error utilerias.playVideo.closed.zf.reveal:");
					console.log(err);
			 	}
			});	


		}catch(err){
			console.log("Error utilerias.playVideoJWP:");
			console.log(err);
		}	
	},
	writeTheNews : function (posts, config) {
		try{
			var bloque 			= posts.bloque1;

			if(!$.isArray(bloque))
				throw "no hay notas para efekto";

			var componentP 		= config.componentP;
			var components 		= $(componentP).find(".cSinglePost");

			if(components.length == 0)
				throw "no hay contenedores";				

			var total 			= bloque.length;
			var items 			= "";

			for(var i = 0; i < total; i++){
				var id 			= bloque[i].ID;
				var time 		= bloque[i].formatTime;
				var formatLink 	= bloque[i].formatLink;
				var guid 		= bloque[i].guid;
				var image 		= bloque[i].image;
				var imageMedium	= image.medium;
				var imageFull	= image.full;
				var imageAlt	= image.title;
				var postExcerpt = bloque[i].post_excerpt;
				var postTitle 	= bloque[i].post_title;
				var videos 		= bloque[i].videos;

				var l 			= 40;
				if(i == 0)
					l 			= 70;

				$(components[i]).find(".txtTitle").attr("title", postTitle);
				$(components[i]).find(".txtTitle").attr("alt", postTitle);
				$(components[i]).find(".txtTitle").attr("href", formatLink);
				$(components[i]).find(".txtTitle h3").html(postTitle);
				$(components[i]).find(".txtTime").html(time);
				$(components[i]).find(".txtTitle100a").html(utilerias.lc(postTitle, l));
				$(components[i]).find(".manageImg").attr("alt", imageAlt);
				$(components[i]).find(".manageImg").attr("title", imageAlt);
				$(components[i]).find(".manageImg").attr("data-original", imageMedium);

				if(videos.trim() == "")
					$(components[i]).find(".iVideo").fadeOut(0);

				$(components[i]).find(".fa-share-alt").attr("data-title", postTitle);
				$(components[i]).find(".fa-share-alt").attr("data-img", imageFull);
				$(components[i]).find(".fa-share-alt").attr("data-link", formatLink);

			}// fin for

			if(total > 0)
				$(config.component).fadeIn(0);


		}catch(err){
			console.log("Error utilerias.writeTheNews:");
			console.log(err);
			$(config.component).fadeOut(0);
		}
	},
	writeCambio : function (posts, config) {
		try{
			var bloque 			= posts.bloque1;

			if(!$.isArray(bloque))
				throw "no hay notas para efekto";

			var componentP 		= config.componentP;
			var components 		= $(componentP).find(".cSinglePost");

			if(components.length == 0)
				throw "no hay contenedores";				

			var total 			= bloque.length;
			var items 			= "";

			for(var i = 0; i < total; i++){
				var id 			= bloque[i].ID;
				var time 		= bloque[i].formatTime;
				var guid 		= bloque[i].guid;
				var formatLink 	= bloque[i].formatLink;
				var image 		= bloque[i].image;
				var imageMedium	= image.medium;
				var imageFull	= image.full;
				var imageAlt	= image.title;
				var postExcerpt = bloque[i].post_excerpt;
				var postTitle 	= bloque[i].post_title;
				var videos 		= bloque[i].videos;

				var l 			= 40;
				if(i == 0)
					l 			= 70;

				$(components[i]).find(".txtTitle").attr("title", postTitle);
				$(components[i]).find(".txtTitle").attr("alt", postTitle);
				$(components[i]).find(".txtTitle").attr("href", formatLink);
				$(components[i]).find(".txtTitle h3").html(postTitle);
				$(components[i]).find(".txtTitle100a").html(utilerias.lc(postTitle, l));
				$(components[i]).find(".txtTime").html(time);
				$(components[i]).find(".manageImg").attr("alt", imageAlt);
				$(components[i]).find(".manageImg").attr("title", imageAlt);
				$(components[i]).find(".manageImg").attr("data-original", imageMedium);

				if(videos.trim() == "")
					$(components[i]).find(".iVideo").fadeOut(0);

				$(components[i]).find(".fa-share-alt").attr("data-title", postTitle);
				$(components[i]).find(".fa-share-alt").attr("data-img", imageFull);
				$(components[i]).find(".fa-share-alt").attr("data-link", formatLink);

			}// fin for

			if(total > 0)
				$(config.component).fadeIn(0);
		}catch(err){
			console.log("Error utilerias.writeCambio:");
			console.log(err);
		}
	},
	writeGaming : function (posts, config) {
		try{
			var bloque 			= posts.bloque1;

			if(!$.isArray(bloque))
				throw "no hay notas para gaming";

			var componentP 		= config.componentP;
			var components 		= $(componentP).find(".cSinglePost");

			if(components.length == 0)
				throw "no hay contenedores";				

			var total 			= bloque.length;
			var items 			= "";


			for(var i = 0; i < total; i++){
				var id 			= bloque[i].ID;
				var time 		= bloque[i].formatTime;
				var guid 		= bloque[i].guid;
				var formatLink 	= bloque[i].formatLink;
				var image 		= bloque[i].image;
				var imageMedium	= image.medium;
				var imageFull	= image.full;
				var imageAlt	= image.title;
				var postExcerpt = bloque[i].post_excerpt;
				var postTitle 	= bloque[i].post_title;

				var l 			= 40;
				if(i == 0)
					l 			= 70;

				$(components[i]).find(".txtTitle").attr("title", postTitle);
				$(components[i]).find(".txtTitle").attr("alt", postTitle);
				$(components[i]).find(".txtTitle").attr("href", formatLink);
				$(components[i]).find(".txtTitle h3").html(postTitle);
				$(components[i]).find(".txtTitle100a").html(utilerias.lc(postTitle, l));
				
				$(components[i]).find(".txtExcerpt").html(postExcerpt);
				$(components[i]).find(".txtTime").html(time);
				$(components[i]).find(".manageImg").attr("alt", imageAlt);
				$(components[i]).find(".manageImg").attr("title", imageAlt);
				$(components[i]).find(".manageImg").attr("data-original", imageMedium);

				$(components[i]).find(".fa-share-alt").attr("data-title", postTitle);
				$(components[i]).find(".fa-share-alt").attr("data-img", imageFull);
				$(components[i]).find(".fa-share-alt").attr("data-link", formatLink);

			}// fin for

			if(total > 0)
				$(config.component).fadeIn(0);
		}catch(err){
			console.log("Error utilerias.writeCambio:");
			console.log(err);
		}
	},
	playVideoInternal : function (component) {
		try{
			var dataJson 		= $(component).attr("data-json").trim();

			if(dataJson == "")
				throw "No hay datos de reproducción";


			dataJson 			= dataJson.split(",");

			if(dataJson.length == 0)
				throw "No hay datos de reproducción";

			$(".swcPlayerVideo .swiper-wrapper").html("");

			var players 		= "";
			for(var i = 0 in dataJson){
				var entryID 	= dataJson[i];

				if(entryID.trim() == "")
					continue;

				var id 			= ("video"+entryID);
				players 		+= 	'<div class="swiper-slide position-relative H100 overflow-hidden swiper-no-swiping">'+
										'<div class="cVideo WA H100 playersKaltura '+id+'" id="'+id+'"></div>'+
									'</div>';

			}// fin for

			if(players.trim() == "")
				throw "No hay nada que reproducir";

			$(".swcPlayerVideo .swiper-wrapper").html(players);

			var prerollUrl 	= "//www5.smartadserver.com/ac?siteid=67472&pgid=517995&fmtid=42830&ab=1&tgt=&oc=1&out=vast4&ps=1&pb=0&visit=S&vcn=s&tmstp=[timestamp]";
		    var midrollUrl 	= "//www5.smartadserver.com/ac?siteid=67472&pgid=517995&fmtid=42830&ab=2&tgt=&oc=1&out=vast4&ps=1&pb=0&visit=S&vcn=s&tmstp=[timestamp]";
		   	var postrollUrl = "//www5.smartadserver.com/ac?siteid=67472&pgid=517995&fmtid=42830&ab=3&tgt=&oc=1&out=vast4&ps=1&pb=0&visit=S&vcn=s&tmstp=[timestamp]";
		   	var overlayUrl	= "//www5.smartadserver.com/ac?siteid=67472&pgid=517995&fmtid=42831&ab=&tgt=&oc=1&out=vast4&ps=1&pb=0&visit=S&vcn=s&tmstp=[timestamp]"; 

			var t;
	        var r       	= false;
	        var s 			= null;

	        s 				= document.createElement('script');
	        s.type      	= 'text/javascript';
	        s.async     	= '';
	        s.defer 		= '';
	        s.src       	= 'https://cdnapisec.kaltura.com/p/1658161/sp/165816100/embedIframeJs/uiconf_id/41532012/partner_id/1658161';

	        s.onload 		= 	s.onreadystatechange = function() {
	                            	/*console.log( this.readyState );*/
	                            	if ( !r && (!this.readyState || this.readyState == 'complete') ){
	                                	r 					= true;

	                                	for(var i = 0 in dataJson){
	                                		var entryID 	= dataJson[i];
											var id 			= ("video"+entryID);

	                                		kWidget.embed({
												'targetId' 	: 	id,
												'wid' 		: 	'_1658161',
												'uiconf_id' : 	'41532012',
												'entry_id' 	: 	entryID,
												'flashvars'	: 	{ 	
													"streamerType" 	: "auto",
													'autoPlay' 		: false,
													/*'myComponent'	: { 
														'plugin' 		: true,
														'iframeHTML5Css':'http://www.efekto.tv/wp-content/themes/EfecktoTV/style.css' 
													},*/
													"comscore"		: {	
														"plugin"	 		: true,
														"position" 			: "before",
														"cTagsMap" 			: "//projects.kaltura.com/ran/comscore/comscore.xml", 
														"trackEventMonitor" : "trackEvent",
														"c2" 				: "19249540",
														"asyncInit" 		: true,
														"relativeTo" 		: "video"
													},
													"vast": {
														"numPreroll" 		: "1",
														"prerollUrl" 		: prerollUrl,
														"numMidroll"		: "1",
														"midrollUrl"        : midrollUrl,
														"numPostroll" 		: "1",
														"postrollUrl" 		: postrollUrl,
														"preSequence" 		: "1",
														"postSequence" 		: "1",
														"overlayStartAt" 	: "20",
														"overlayInterval" 	: "300",
														"overlayUrl" 		: overlayUrl,
														"timeout" 			: "4",
														"prerollInterval" 	: "1",
														"prerollStartWith" 	: "1",
														"postrollInterval" 	: "1",
														"postrollStartWith" : "1"
													},
													"adsOnReplay" 		: true,
													"inlineScript" 		: false
												},
												'params'	: 	{ 'wmode' : 'transparent' }
											});	
	                                	}
	                                	

										$("#cPlayerVideo").foundation('open');

										var swiper = new Swiper('.swcPlayerVideo', {
									      	pagination : {
									        	el 		: '.swiper-pagination',
									        	type 	: 'progressbar',
									      	},
									      	navigation : {
									        	nextEl 	: '.swNextPlayerVideo',
									        	prevEl 	: '.swPrevPlayerVideo',
									      	},
									      	noSwiping 	: true,

									    });	

									    kWidget.addReadyCallback( function( playerId ){
											$('.swNextPlayerVideo, .swPrevPlayerVideo').click(function(){
												var kdp = document.getElementsByClassName("playersKaltura");
												for(var i = 0 in kdp){
													kdp[i].sendNotification( "doPause" );
												}
											});
										});

									    $("#cPlayerVideo").off();
										$("#cPlayerVideo").on('closed.zf.reveal', function () {
					                   	 	try{
					                   	 		$("#cPlayerVideo #cScriptVideoPlayer").html("");
					                   	 		$("#cPlayerVideo .swcPlayerVideo .swiper-wrapper").html("");
					                   	 	}catch(err){
					                   	 		console.log("Error utilerias.playVideo.closed.zf.reveal:");
												console.log(err);
					                   	 	}
					                	});	

	                            	}
	                        	};

	        t           	= document.getElementById("cScriptVideoPlayer");
	        t.appendChild(s); 

		}catch(err){
			console.log("Error utilerias.playVideoInternal:");
			console.log(err);
		}
	},
	validations : function () {
		try{
			if($(document).find("#formContacto").length > 0){
				$('#formContacto').bootstrapValidator({
					message 		: 	'Datos no validos',
					excluded 		: 	':disabled, :hidden, :not(:visible)',
					fields 			: 	{
						nombre 	: 	{
							validators 	: 	{
								notEmpty 	: 	{
									message 	: 	'Coloca tu nombre completo'
								},
								stringLength 	: 	{
									min 	: 	3,
									max 	: 	50,
									message : 	'El nombre debe tener mínimo 3 y máximo 50 caracteres de longitud'
								}
							}
						},
						correo 	: 	{
							validators 	: 	{
								notEmpty: {
				                    message 	: 'Coloca un correo válido'
				                },
								emailAddress 	: 	{
									message 	: 	'Coloca un correo válido'
								},
								stringLength 	: 	{
									min 	: 	7,
									max 	: 	50,
									message : 	'El correo debe tener mínimo 7 y máximo 50 caracteres de longitud'
								}
							}
						},
						numero 	: 	{
							validators 	: 	{
								stringLength 	: 	{
									min 	: 	10,
									max 	: 	10,
									message : 	'El número de contacto debe tener 10 digitos'
								},
			                    integer: {
			                        message : 	'Número de contacto invalido'
			                    }
							}
						},
						compania 	: 	{
							validators 	: 	{
								stringLength 	: 	{
									min 	: 	2,
									max 	: 	50,
									message : 	'El nombre de la compañía debe tener mínimo 2 y máximo 50 caracteres de longitud'
								}
							}
						},
						asunto 	: 	{
							validators 	: 	{
								notEmpty 	: 	{
									message 	: 	'Asunto a tratar'
								},
								stringLength 	: 	{
									min 	: 	3,
									max 	: 	50,
									message : 	'El asunto debe tener mínimo 3 y máximo 50 caracteres de longitud'
								}
							}
						},
						mensaje 	: 	{
							validators 	: 	{
								notEmpty 	: 	{
									message 	: '¿En qué podemos ayudarte?'
								},
								stringLength 	: 	{
									min 	: 	10,
									max 	: 	500,
									message : 	'El mensaje debe tener mínimo 10 y máximo 500 caracteres de longitud'
								}
							}
						}
					}
				}).on('status.field.bv', function(e, data) {
					try{
						utilerias.responseError(e, data);
					}catch(err){
						console.log("ERROR JS utilerias.validations.formContacto.status: "+err);
					}
				}).on('success.form.bv', function(e) {
					try{
						// e.preventDefault();

						// if($(e.target)[0].id == "formContacto")
						// 	utilerias.sendMessage();
					}catch(err){
						console.log("ERROR JS utilerias.validations.formContacto.success: "+err);
					}
		        }).on('error.form.bv', function(e) {
			        
			    });
			}

			if($(document).find("#formShare").length > 0){
				$('#formShare').bootstrapValidator({
					message 		: 	'Datos no validos',
					excluded 		: 	':disabled, :hidden, :not(:visible)',
					fields 			: 	{
						contacto 	: 	{
							validators 	: 	{
								notEmpty 	: 	{
									message 	: 	'Coloca el contacto'
								},
								stringLength 	: 	{
									min 	: 	3,
									max 	: 	50,
									message : 	'El contacto debe tener mínimo 3 y máximo 50 caracteres de longitud'
								}
							}
						},
						correo 	: 	{
							validators 	: 	{
								notEmpty: {
				                    message 	: 'Coloca un correo válido'
				                },
								emailAddress 	: 	{
									message 	: 	'Coloca un correo válido'
								},
								stringLength 	: 	{
									min 	: 	7,
									max 	: 	50,
									message : 	'El correo debe tener mínimo 7 y máximo 50 caracteres de longitud'
								}
							}
						}
					}
				}).on('status.field.bv', function(e, data) {
					try{
						utilerias.responseError(e, data);
					}catch(err){
						console.log("ERROR JS utilerias.validations.formContacto.status: "+err);
					}
				}).on('success.form.bv', function(e) {
					try{
						// e.preventDefault();

						// if($(e.target)[0].id == "formContacto")
						// 	utilerias.sendMessage();
					}catch(err){
						console.log("ERROR JS utilerias.validations.formContacto.success: "+err);
					}
		        }).on('error.form.bv', function(e) {
			        isValidGeneral = false;
			    });
			}
		}catch(err){
			console.log("Error utilerias.validations:");
			console.log(err);
		}
	},
	responseError : function (e, data) {
		try{
			var $form 		= $(e.target),
			validator 		= data.bv,
			$collapsePane 	= data.element.parents('.collapse'),
			colId 			= $collapsePane.attr('id');

			if (colId) {
				var $anchor = $('a[href="#' + colId + '"][data-toggle="collapse"]');
				var $icon 	= $anchor.find('i');

				// Add custom class to panel containing the field
				if (data.status == validator.STATUS_INVALID) {
					$anchor.addClass('bv-col-error');
					$icon.removeClass(faIcon.valid).addClass(faIcon.invalid);
				} else if (data.status == validator.STATUS_VALID) {
					var isValidCol = validator.isValidContainer($collapsePane);
					if (isValidCol) {
						$anchor.removeClass('bv-col-error');
					}else{
						$anchor.addClass('bv-col-error');
					}
					$icon.removeClass(faIcon.valid + " " + faIcon.invalid).addClass(isValidCol ? faIcon.valid : faIcon.invalid);
				}
			}
		}catch(err){
			console.log("ERROR JS utilerias.responseError: ");
			console.log(err);
		}
	},
	sendMessage : function () {
		try{
			var form 	= $("#formContacto").serializeArray();

			$.ajax({
			 	type 			: "POST",
			    url 			: ajaxurl,
			    data 			: { "action" : "contact-us1" }, 
			    dataType 		: 'html',
			    processData 	: false,
				beforeSend 		: function(){ },
			    success 		: function (response) {
			     	try{
			     		console.log(response);
			   //   		response 	= JSON.parse(response);
						// if(response.success == 2)
						//  	throw response.message;

						// var db 		= response.db;
						// utilerias.setView(db);
			     	}catch(err){
						console.log("Error JS utilerias.sendMessage.ajax.success:");
						console.log(err);
			     	}
			     },
			     error(xhr,status,error){
			     	console.log("Error JS utilerias.sendMessage.ajax.error: ");
			     	console.log(xhr);
			     	console.log(status);
			     	console.log(error);
			     	console.log(xhr.responseText);
			     }
			});


		}catch(err){
			console.log("ERROR JS utilerias.sendMessage: ");
			console.log(err);
		}
	},
	showFrecuencias : function (component) {
		try{
			$(".cFrecuencias").fadeOut(0);
			var estacion 	= $(component).attr("data-estacion");
			$(estacion).fadeIn(300);

			$(".cControlsPlayer").removeClass("bgs123a bgs126a bgs124a bgs125a hvc107a hvc105a hvc109a hvc111a");
			$(".cPlayerOthers").removeClass("col119a col146a col145a col148a");
			$(".controlPlayer i").removeClass("hvc106a hvc104a hvc108a hvc110a");

			switch(estacion){
				case "#rc":
					$(".cControlsPlayer").addClass("bgs124a hvc107a");
					$(".cPlayerOthers").addClass("col146a");
					$(".controlPlayer i").addClass("hvc106a");
					break;
				case "#cf":
					$(".cControlsPlayer").addClass("bgs123a hvc105a");
					$(".cPlayerOthers").addClass("col145a");
					$(".controlPlayer i").addClass("hvc104a");
					break;
				case "#cm":
					$(".cControlsPlayer").addClass("bgs125a hvc109a");
					$(".cPlayerOthers").addClass("col148a");
					$(".controlPlayer i").addClass("hvc108a");
					break;
				case "#cp":
					$(".cControlsPlayer").addClass("bgs126a hvc111a"); 
					$(".cPlayerOthers").addClass("col119a");
					$(".controlPlayer i").addClass("hvc110a");
					break;
				default: break;
			}

		}catch(err){
			console.log("ERROR JS utilerias.showFrecuencias: ");
			console.log(err);
		}
	},
	closeListFrecuencias : function (component) {
		try{
			$(".cFrecuencias").fadeOut(0);
		}catch(err){
			console.log("ERROR JS utilerias.closeListFrecuencias: ");
			console.log(err);
		}
	},
	loadTriton : function () {
		try{
			var t;
            var r       = false;
            var s       = document.createElement('script');
            s.type      = 'text/javascript';
            s.async     = '';
            s.charset   = "utf-8";
            s.src       = "https://sdk.listenlive.co/web/2.9/td-sdk.min.js";
            s.onload    =   s.onreadystatechange = function() {
                                //console.log( this.readyState ); //uncomment this line to see which ready states are called.
                                if ( !r && (!this.readyState || this.readyState == 'complete') ){
                                    r = true;
                                    
                                   utilerias.initTriton();
                                }
                            };
            t           = document.getElementsByTagName('script')[0];
            t.parentNode.insertBefore(s, t);

		}catch(err){
			console.log("ERROR JS utilerias.loadTriton: ");
			console.log(err);
		}
	},
	initTriton 	: function () {
		try{
			 
			var tdPlayerConfig  =   {
                coreModules         :   [{
                                            id              : 'MediaPlayer',
                                            playerId        : 'td_container',
                                            techPriority    : ['Html5', 'Flash']
                                        }],
                playerReady         : utilerias.onPlayerReady,
                configurationError  : utilerias.onConfigurationError,
                moduleError         : utilerias.onModuleError,
                adBlockerDetected   : utilerias.onAdBlockerDetected,
                timeShift           : { active  : 1, max_listening_time: 30 }
            };
     		
            //Player instance
            playerTriton  = new TDSdk( tdPlayerConfig );


		}catch(err){
			console.log("ERROR JS utilerias.initTriton: ");
			console.log(err);
		}
	},
	onPlayerReady : function () {
		try{
			playerTriton.addEventListener( 'track-cue-point', 				utilerias.onTrackCuePoint );
            playerTriton.addEventListener( 'player-ready', 					utilerias.playerReady );
            playerTriton.addEventListener( 'stream-start', 					utilerias.onStreamStart );
            playerTriton.addEventListener( 'stream-stop', 					utilerias.onStreamStop );
            playerTriton.addEventListener( 'time-shift-playhead-update', 	utilerias.onTimeShiftPlayheadUpdate );
            playerTriton.addEventListener( 'list-loaded', 					utilerias.onListLoaded );
            playerTriton.addEventListener( 'stream-config-ready', 			utilerias.onStreamConfigReady );
            playerTriton.addEventListener( 'stream-config-load-error', 		utilerias.onStreamConfigLoadError );
            playerTriton.addEventListener( 'stream-status', 				utilerias.onStreamStatus );

            utilerias.verifyReload();
		}catch(err){
			console.log("ERROR JS utilerias.onPlayerReady: ");
			console.log(err);
		}
	},
	onConfigurationError : function (e) {
		try{
			console.log("utilerias.onConfigurationError:");
            console.log(JSON.stringify(e));
		}catch(err){
			console.log("ERROR JS utilerias.onConfigurationError: ");
			console.log(err);
		}
	},
	onModuleError : function (e) {
		try{
			console.log("utilerias.onModuleError:");
            console.log(JSON.stringify(e));
		}catch(err){
			console.log("ERROR JS utilerias.onModuleError: ");
			console.log(err);
		}
	},
	onAdBlockerDetected : function () {
		try{
			console.log('AdBlockerDetected');
		}catch(err){
			console.log("ERROR JS utilerias.onAdBlockerDetected: ");
			console.log(err);
		}
	},
    onTrackCuePoint : function(e){
        try{
            console.log('Artist: ' + e.data.cuePoint.artistName + '\nTitle: ' + e.data.cuePoint.cueTitle);
        }catch(err){
            console.log("ERROR JS utilerias.onTrackCuePoint: ");
			console.log(err);
        }
    },
    playerReady : function(){
        try{
            console.log("Player Ready");
        }catch(err){
            console.log("ERROR JS utilerias.playerReady: ");
			console.log(err);
        }
    },
    onStreamStart : function(){
        try{
            
        }catch(err){
            console.log("ERROR JS utilerias.onStreamStart: ");
			console.log(err);
        }
    },
    onStreamStop : function(){
        try{
        	console.log("STOP STREAMING");
            $(".controlPlayer").attr("data-control", 0);
           	$(".cPlayePause").fadeOut(0);
           	$(".cPlayePlay").fadeIn(0);
           	//$(".cPlayeStop").fadeOut(0);
           	//$(".cPlayerOthers").fadeOut(0);
           	$(".cPlayerStatus").html("Stop");
           	$(".bPlayer").fadeOut(0);
        }catch(err){
            console.log("ERROR JS utilerias.onStreamStop: ");
			console.log(err);
        }
    },
    onTimeShiftPlayheadUpdate : function(e){
        try{
        	console.log('utilerias.onTimeShiftPlayheadUpdate:');
           	console.log(e);
            //console.log( 'TIEMPO: ' + JSON.stringify(e.playingTime));
        }catch(err){
            console.log("ERROR JS utilerias.onTimeShiftPlayheadUpdate: ");
			console.log(err);
        }
    },
    onListLoaded : function(e){
        try{
            // console.log('utilerias.onListLoaded');
            // console.log(e);
        }catch(err){
            console.log("ERROR JS utilerias.onListLoaded: ");
			console.log(err);
        }
    },
    onStreamConfigReady : function(e){
        try{
            // console.log('utilerias.onStreamConfigReady:');
           	// console.log(e);
        }catch(err){
            console.log("ERROR JS utilerias.onStreamConfigReady: ");
			console.log(err);
        }
    },
    onStreamConfigLoadError : function(e){
        try{
            // console.log('utilerias.onStreamConfigLoadError:');
           	// console.log(e);
        }catch(err){
            console.log("ERROR JS utilerias.onStreamConfigLoadError: ");
			console.log(err);
        }
    },
    onStreamStatus : function(e){
        try{
           	var code 	= e.data.code;
           	switch(code){
           		case 'LIVE_PAUSE': 			// Paused
           			$(".controlPlayer").attr("data-control", 0);
           			$(".cPlayePause").fadeOut(0);
           			$(".cPlayePlay").fadeIn(0);
           			$(".cPlayeStop").fadeIn(0);
           			$(".cPlayerOthers").fadeIn(0);
           			$(".cPlayerStatus").html("Pausa");
           			break;

           		case 'LIVE_PLAYING': 		// On Air
           			$(".controlPlayer").attr("data-control", 1);
           			$(".cPlayePlay").fadeOut(0);
           			$(".cPlayePause").fadeIn(0);
           			$(".cPlayeStop").fadeIn(0);
           			$(".cPlayerOthers").fadeIn(0);
           			$(".cPlayerStatus").html('<i class="fas fa-circle fSize12 col147a"></i> On Air');
           			$(".bPlayer").fadeIn(0);
           			break;

           		case 'LIVE_STOP': 			// Disconnected
           			$(".controlPlayer").attr("data-control", 0);
           			$(".cPlayePause").fadeOut(0);
           			$(".cPlayePlay").fadeIn(0);
           			$(".cPlayeStop").fadeIn(0);
           			$(".cPlayerOthers").fadeIn(0);
           			$(".cPlayerStatus").html("Desconectado");
           			break;

           		case 'LIVE_FAILED': 		// Stream unavailable
           			$(".controlPlayer").attr("data-control", 0);
           			$(".cPlayePause").fadeOut(0);
           			$(".cPlayePlay").fadeIn(0);
           			$(".cPlayeStop").fadeIn(0);
           			$(".cPlayerOthers").fadeIn(0);
           			$(".cPlayerStatus").html("No disponible");
           			$(".bPlayer").fadeOut(0);
           			break;

           		case 'LIVE_BUFFERING': 		// Buffering...
           			$(".cPlayerStatus").html("Buffering...");
           			break;

           		case 'LIVE_CONNECTING': 	// Live stream connection in progress...
           			$(".cPlayerStatus").html("Conexión live stream...");
           			break;

           		case 'LIVE_RECONNECTING': 	// Will reconnect live stream in x seconds
           			$(".cPlayerStatus").html("Reconectado...");
           			break;

           		case 'STATION_NOT_FOUND': 	// Station not found
           			$(".controlPlayer").attr("data-control", 0);
           			$(".cPlayePause").fadeOut(0);
           			$(".cPlayePlay").fadeIn(0);
           			$(".cPlayeStop").fadeIn(0);
           			$(".cPlayerOthers").fadeIn(0);
           			$(".cPlayerStatus").html("No disponible");
           			$(".bPlayer").fadeOut(0);
           			break;

           		default: break;
           	}

        }catch(err){
            console.log("ERROR JS utilerias.onStreamStatus: ");
			console.log(err);
        }
    },
	playPauseStreaming : function (component) {
		try{
			var sigla 		= $(component).attr("data-sigla").trim();
			var control 	= $(component).attr("data-control").trim();
			var nombre 		= $(component).children('span').text();

			var data 		= {
				"sigla" 	: sigla,
				"nombre" 	: nombre
			}

			if(sigla == "")
				throw ""; 

			if(nombre)
				$(".cPlayerFrecuencia").html(nombre);

			if(playerTriton == null){
				utilerias.initTriton();
			}else{
				if(utilerias.verifyPlaying(data)){
				 	playerTriton.play( { station: sigla } );
				}else{
					if(parseInt(control) == 0){
						playerTriton.play( { station: sigla } );
					}else{
						playerTriton.pause();
					}
				}
			}

			$(".cPlayePlay, .cPlayePause").attr("data-sigla", sigla);
		}catch(err){
			console.log("ERROR JS utilerias.playPauseStreaming: ");
			console.log(err);
		}
	},
	stopStreaming : function (component) {
		try{

			if(playerTriton == null)
				throw "No hay nada que detener";

			playerTriton.stop();

			if (typeof(Storage) === "undefined")
				throw "No hay variable en memoria";

			var playing 	= sessionStorage.getItem("frecuency");

			if(playing == null)
				throw "No hay variable en memoria";

			sessionStorage.removeItem("frecuency");

		}catch(err){
			console.log("ERROR JS utilerias.stopStreaming: ");
			console.log(err);
		}	
	},
	verifyPlaying : function (data) {
		var result 			= false;
		try{
			// true para hacer el cambio de estacion o iniciar reproduccion
			if (typeof(Storage) === "undefined")
				result 		= true;

			var save 		= encodeURIComponent(JSON.stringify(data));
			var playing 	= sessionStorage.getItem("frecuency");

			if(playing == null){
				sessionStorage.setItem("frecuency", save);
				result 		= true;
			}else{
				playing 	= JSON.parse(decodeURIComponent(playing));
				if(playing.sigla != data.sigla){
					sessionStorage.setItem("frecuency", save);
					result 	= true;
				}
			}

		}catch(err){
			console.log("ERROR JS utilerias.verifyPlaying: ");
			console.log(err);
		}
		return result;
	},
	verifyReload : function () {
		try{
			if (typeof(Storage) === "undefined")
				result 		= true;

			var playing 	= sessionStorage.getItem("frecuency");
			if(playing == null)
				throw "";

			playing 		= JSON.parse(decodeURIComponent(playing));

			playerTriton.play( { station: playing.sigla } );
			$(".cPlayerFrecuencia").html(playing.nombre);
			$(".cPlayePlay, .cPlayePause").attr("data-sigla", playing.sigla);

		}catch(err){
			console.log("ERROR JS utilerias.verifyReload: ");
			console.log(err);
		}
	},
	ctrlSearh : function () {
		try{
			$("#cSearch").fadeToggle();
		}catch(err){
			console.log("ERROR JS utilerias.ctrlSearh: ");
			console.log(err);
		}
	},

	alertPost : function () {
		try{
			var scAlertPost = null;
			$.ajax({
			 	type 			: "POST",
			 	dataType 		: "json",
			    url 			: "http://localhost/CapitalMexico/wp-content/plugins/AlertPost/files/sevices.php",
			    data 			: { "file" : config.plugin },
				beforeSend 		: function(){  },
			     success 		: function (success) {
			     	try{
			     		if(parseInt(success.success) == 2)
			     			throw success.message;
			     		
			     		var db 			= success.db;
			     		var items 		= "";
			     		for(var i = 0 in db){
			     			var title 	= db[i].post_title.replace(/\\"/g, "");
			     			items 	+= '<div class="swiper-slide">'+
			     							'<div class="grid-container W100">'+
								      		'<div class="grid-x grid-padding-x W100 large-padding-collapse medium-padding-collapse small-padding-collapse">'+
								      			'<div class="cell large-11 medium-11 small-12 border-box PLR10">'+
								      				'<a href="'+db[i].guid+'" title="'+title+'" alt="'+title+'"><h3 class="BentonSansComp-Medium fSize16 col135a tranText100a">'+title+'</h3></a>'+
								      			'</div>'+
								      			'<div class="cell large-1 medium-1 small-12 text-center">'+
								      				'<i class="fas fa-share-alt fSize16 col135a cPointer" data-title="'+title+'" data-img="'+db[i].image+'" data-link="'+db[i].guid+'" onclick="utilerias.sharePost(this); return false;"></i>'+
								      			'</div>'+
								      		'</div>'+
								      		'</div>'+
								      	'</div>';
			     		}

			     		$("#cAlertPost").find(".swiper-wrapper").html(items);
			     		
			     		if(items != ""){
			     			$("#cAlertPost").fadeIn(0);

			     			if(db.length == 1){
			     				scAlertPost = new Swiper('.scAlertPost', {
							      	slidesPerView 		: 1, /*columnas*/
							      	centeredSlides 		: true,
							      	spaceBetween 		: 0
							    });
							    utilerias.newFindAlertPost(scAlertPost);
			     			}else{
			     				scAlertPost = new Swiper('.scAlertPost', {
							      	slidesPerView 		: 1, /*columnas*/
							      	centeredSlides 		: true,
							      	spaceBetween 		: 0,
							      	speed 				: 800,
							      	autoplay 			: {
							        	delay 					: 7000,
							        	disableOnInteraction 	: false
							      	},
							      	on 					: {
									    reachEnd 	: function () {
									    	try{
									    		scAlertPost.autoplay.stop();
									    		utilerias.newFindAlertPost(scAlertPost);
									    	}catch(err){
									    		console.log('utilerias.alertPost.on.reachEnd:');
												console.log(err);
									    	}
									    }
									}
							    });
			     			}
			     		}else{
			     			$("#cAlertPost").fadeOut(0);
			     			utilerias.newFindAlertPost(scAlertPost);
			     		}
			     	}catch(err){
			     		console.log("Error utilerias.alertPost.ajax.success:");
						console.log(err);
						$("#cAlertPost").fadeOut(0);
						utilerias.newFindAlertPost(scAlertPost);
			     	}
			     },
			     error(xhr,status,error){
			     	console.log("ERROR JS utilerias.alertPost.ajax.error: ");
			     	console.log(xhr.responseText);
			     	$("#cAlertPost").fadeOut(0);
			     	utilerias.newFindAlertPost(scAlertPost);
			     }
			 });
		}catch(err){
			console.log("Error utilerias.alertPost.ajax.success:");
			console.log(err);
			$("#cAlertPost").fadeOut(0);
			utilerias.newFindAlertPost();
		}
	},
	newFindAlertPost : function (scAlertPost) {
		try{
			setTimeout(function(){ 
				if(scAlertPost != null){
					scAlertPost.detachEvents();
					scAlertPost.destroy(true, true);
				}

				utilerias.alertPost();
			}, 7000); // 7 seg
		}catch(err){
			console.log("Error utilerias.newFindAlertPost:");
			console.log(err);
		}
	},
	lc : function (str, long) {
		try{
			var l 	= str.length;

			if(long >= l)
				return str;

			str 	= str.substring(0, long)+"...";
			return str;
		}catch(err){
			console.log("Error utilerias.lc:");
			console.log(err);
		}
	},
	viewImageDestacada : function (component) {
		try{
			var dataExcerpt 		= $(component).attr("data-excerpt").trim();
			var dataImage 			= $(component).attr("data-img").trim();

			if(dataImage == "" )
				throw "No hay datos que mostrar";

			$("#dataViewDestacada #cImage img").attr("src", dataImage);
			$("#dataViewDestacada #cImage img").attr("alt", dataExcerpt);
			$("#dataViewDestacada #cImage img").attr("title", dataExcerpt);
			$("#dataViewDestacada #cCreditos p").html(dataExcerpt);
			$("#dataViewDestacada").foundation('open');

		}catch(err){
			console.log("Error utilerias.viewImageDestacada:");
			console.log(err);
		}
	},
	removeGallerySingle : function () {
		try{
			$("#cContentSingle").find(".gallery").remove();
		}catch(err){
			console.log("Error utilerias.removeGallerySingle:");
			console.log(err);
		}
	},
	showMore : function () {
		try{
			if(!$("#morePosts").is(":visible"))
				$("#morePosts").fadeIn(0);
		}catch(err){
			console.log("Error utilerias.showMore:");
			console.log(err);
		}
	},
	loadSmartBanners : function () {
		try{
			// CAPITAL MEXICO
            var config 			= {
            	"dataSmart"	: "67472/517995",
            	"siteID" 	: 67472,
            	"pageID" 	: 517995,
            	"formats" 	: [
            		{ "id" : 70856 },
					{ "id" : 70741 },
					{ "id" : 70742 },
					{ "id" : 70857 },
					{ "id" : 70853 },
					{ "id" : 70854 },
					{ "id" : 70869 },
					{ "id" : 70870 },
					{ "id" : 70871 },
					{ "id" : 70740 }
            	]
            }

			var section 		= $("#sectionSite").val().trim();

			if(section != ""){
				section 		= JSON.parse(decodeURIComponent(section));

				if(parseInt(section.page) != 0){
					var name 	= section.name;

					var sites 	= [
						{
							"name" 		: "aguascalientes",
			            	"dataSmart"	: "67472/1007797",
			            	"pageID" 	: 1007797
			            },
			            {
							"name" 		: "baja-california",
			            	"dataSmart"	: "67472/1007798",
			            	"pageID" 	: 1007798
			            },
			            {
							"name" 		: "baja-california-sur",
			            	"dataSmart"	: "67472/1007799",
			            	"pageID" 	: 1007799
			            },
			            {
							"name" 		: "campeche",
			            	"dataSmart"	: "67472/1007801",
			            	"pageID" 	: 1007801
			            },
			            {
							"name" 		: "chiapas",
			            	"dataSmart"	: "67472/1007802",
			            	"pageID" 	: 1007802
			            },
			            {
							"name" 		: "chihuahua",
			            	"dataSmart"	: "67472/1007803",
			            	"pageID" 	: 1007803
			            },
			            {
							"name" 		: "coahuila",
			            	"dataSmart"	: "67472/1007804",
			            	"pageID" 	: 1007804
			            },
			            {
							"name" 		: "colima",
			            	"dataSmart"	: "67472/1007805",
			            	"pageID" 	: 1007805
			            },
			            {
							"name" 		: "durango",
			            	"dataSmart"	: "67472/1007796",
			            	"pageID" 	: 1007796
			            },
			            {
							"name" 		: "edomex",
			            	"dataSmart"	: "67472/1007806",
			            	"pageID" 	: 1007806
			            },
			            {
							"name" 		: "guanajuato",
			            	"dataSmart"	: "67472/1007807",
			            	"pageID" 	: 1007807
			            },
			            {
							"name" 		: "guerrero",
			            	"dataSmart"	: "67472/1007808",
			            	"pageID" 	: 1007808
			            },
			            {
							"name" 		: "hidalgo",
			            	"dataSmart"	: "67472/1007809",
			            	"pageID" 	: 1007809
			            },
			            {
							"name" 		: "jalisco",
			            	"dataSmart"	: "67472/1007810",
			            	"pageID" 	: 1007810
			            },
			            {
							"name" 		: "michoacan",
			            	"dataSmart"	: "67472/1007827",
			            	"pageID" 	: 1007827
			            },
			            {
							"name" 		: "morelos",
			            	"dataSmart"	: "67472/1007812",
			            	"pageID" 	: 1007812
			            },
			            {
							"name" 		: "nayarit",
			            	"dataSmart"	: "67472/1007813",
			            	"pageID" 	: 1007813
			            },
			            {
							"name" 		: "nuevo-leon",
			            	"dataSmart"	: "67472/1007814",
			            	"pageID" 	: 1007814
			            },
			            {
							"name" 		: "oaxaca",
			            	"dataSmart"	: "67472/1007815",
			            	"pageID" 	: 1007815
			            },
			            {
							"name" 		: "puebla",
			            	"dataSmart"	: "67472/1007816",
			            	"pageID" 	: 1007816
			            },
			            {
							"name" 		: "queretaro",
			            	"dataSmart"	: "67472/1007817",
			            	"pageID" 	: 1007817
			            },
			            {
							"name" 		: "quintana-roo",
			            	"dataSmart"	: "67472/1007818",
			            	"pageID" 	: 1007818
			            },
			            {
							"name" 		: "san-luis-potosi",
			            	"dataSmart"	: "67472/1007819",
			            	"pageID" 	: 1007819
			            },
			            {
							"name" 		: "sinaloa",
			            	"dataSmart"	: "67472/1007820",
			            	"pageID" 	: 1007820
			            },
			            {
							"name" 		: "sonora",
			            	"dataSmart"	: "67472/1007821",
			            	"pageID" 	: 1007821
			            },
			            {
							"name" 		: "tabasco",
			            	"dataSmart"	: "67472/1007822",
			            	"pageID" 	: 1007822
			            },
			            {
							"name" 		: "tamaulipas",
			            	"dataSmart"	: "67472/1007823",
			            	"pageID" 	: 1007823
			            },
			            {
							"name" 		: "tlaxcala",
			            	"dataSmart"	: "67472/1007824",
			            	"pageID" 	: 1007824
			            },
			            {
							"name" 		: "veracruz",
			            	"dataSmart"	: "67472/1007825",
			            	"pageID" 	: 1007825
			            },
			            {
							"name" 		: "yucatan",
			            	"dataSmart"	: "67472/1007826",
			            	"pageID" 	: 1007826
			            }
					]


					for(var i = 0 in sites){
						if(name == sites[i].name){
							config.dataSmart 	= sites[i].dataSmart;
							config.pageID 		= sites[i].pageID;
							config.formats 		= [
								{ "id" : 70857 },
								{ "id" : 70853 },
								{ "id" : 70854 },
								{ "id" : 70869 },
								{ "id" : 70870 },
								{ "id" : 70871 },
								{ "id" : 70740 }
							]
							break;
						}
					}
				}
			}

			sas.setup({ domain: 'https://www5.smartadserver.com', async: true, renderMode: 1});

			sas.cmd.push(function() {
				var formats 		= config.formats;
				for(var i = 0 in formats){
					var formatID 	= formats[i].id;

					if(($(document).find( ("#sas_"+formatID) ).length) == 0)
						continue;
					
				    sas.call("std", {
				        siteId 		: (parseInt(config.siteID)),
				        pageId 		: (parseInt(config.pageID)),
				        formatId 	: (parseInt(formatID)),
				        target 		: ''
				    });
				}
			});
			
		}catch(err){
			console.log("Error utilerias.loadSmartBanners:");
			console.log(err);
		}
	},
	modalSearch : function () {
		try{
			$("#dataSearch").foundation('open');
		}catch(err){
			console.log("Error utilerias.modalSearch:");
			console.log(err);
		}
	},
	loadNewswire : function () {
		try{
			if($(document).find("#w13394_widget").length == 0)
				throw "No hay widget newswire que cargar";
			
			var _wsc = document.createElement('script');
			_wsc.src = '//tools.prnewswire.com/es/live/13394/widget.js';
			document.getElementsByTagName("Head")[0].appendChild(_wsc);

		}catch(err){
			console.log("Error utilerias.loadNewswire:");
			console.log(err);
		}
	}
}



