<br>
<div class="row">
	<div class="columns large-12">
		<div class="top-bar">
		  <div class="top-bar-left">
		    <ul class="dropdown menu" data-dropdown-menu>
		      <li class="menu-text"><h1>Welcome <?= $response[0]->username; ?></h1></li>
		      <li><a href="#">Two</a></li>
		      <li><a href="#">Three</a></li>
		    </ul>
		  </div>
		  <div class="top-bar-right">
		    <ul class="menu">
		      <li><input type="search" placeholder="Search"></li>
		      <li><button type="button" class="button">Search</button></li>
		    </ul>
		  </div>
		</div>
	</div>
</div>
<div class="row"></div>

<div class="row">
	<br>

	<a href="<?= base_url(); ?>index.php/FortniteClans/verifique">
		<button class="button">Add User</button>
	</a>

  	<br>
	  	<div class="columns small-6 large-4">
	  		<table class="table">
	  			<thead>
	  				<tr>
	  					<th>User Name</th>
	  					<th>Email</th>
	  					<th>Action</th>
	  				</tr>
	  			</thead>
	  			<tbody>
	  				<?php foreach($response as $i){ ?>
	  					<tr>
	  						<td><?= $i->username; ?></td>
	  						<td><?= $i->email; ?></td>
	  						<td>
	  							<a href="<?= base_url(); ?>index.php/FortniteClans/editUser/<?= $i->id_user ?>">
	  								<button type="button" class="button">Edit</button>
	  							</a>
	  							<a>
	  								<button type="button" class="alert button">Remove</button>
	  							</a>
	  						</td>
	  					</tr>
	  				<?php } ?>	
	  			</tbody>
	  		</table>
	  	</div>
  	
</div>
