<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title> <?php echo $title; ?></title>	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/foundation/css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">		
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/swiper/dist/css/swiper.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/fontawesome/css/fontawesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/fontawesome/css/all.css">
	<!--<script src='https://cloud.tinymce.com/5/tinymce.min.js?apiKey=9up2pc428yt2no7et1qrjzi7xb3fkk6kaioejm7alhlk7e8j'></script>
  	<script>
  	tinymce.init({
  	  selector: '#mytextarea'
  	});
  	</script>-->
</head>


<header>
	<div class="grid-container full bgg101a">
		<div class="grid-x grid-padding-x large-up-7 HPX80 align-middle fSize22 col100a text-center ">
			<div class="cell cPointer hvts100a H100 PT25 tranText100a">Home</div>
			<div class="cell cPointer hvts100a H100 PT25 tranText100a">Busco Clan</div>
			<div class="cell cPointer hvts100a H100 PT25 tranText100a">Reclutar</div>
			<div class="cell cPointer hvts100a H100 PT25 tranText100a">Torneos</div>
			<div class="cell cPointer hvts100a H100 PT25 tranText100a">Estadisticas</div>
			<div class="cell cPointer hvts100a H100 PT25 tranText100a">Practica</div>
			<div class="cell cPointer hvts100a H100 PT25 tranText100a">Clips</div>
		</div>
	</div>
</header>
