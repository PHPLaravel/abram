<body class="bgi100a">



<div class="grid-container">
	<div class="grid-x grid-margin-x HPX900">		
		<div class="cell large-8 bgs103a padding-top-3">
			<h1 class="Fortnite-Font fSize40 text-center col105a">Crear mi Clan</h1>
			<form method="post" action="<?= base_url();?>index.php/FortniteClans/salvarClan" id="formulario"  enctype="multipart/form-data">
			    <div class="grid-x grid-padding-x align-center margin-top-3">
			      <div class="medium-10">
			      	<label>
			      		Profile
			      		<input type="file" id="profile_image" name="profile_image" size="33" />
			      	</label>
			      </div>		
			      <div class="medium-10 cell">
			        <label class="fSize16 OpenSans-Regular col106a">Nombre del Clan:*
			          <input type="text" placeholder="" name="nombre" id="nombreinput" required>
			        </label>
			      </div>

			    <div class="cell medium-10">
			    	<legend class="text-left fSize16 OpenSans-Regular col106a">Requisitos:</legend>    	
			    </div>
			      
			    <div class="medium-3 cell">

      			  	<label class="OpenSans-Regular col106a">Número de victorias
      			    	<input type="text" placeholder="#" name="victorias" id="victoriasinput" >
      			  	</label>
      			</div>
      			<div class="medium-3 cell">
      			  	<label class="OpenSans-Regular col106a">Horario
      			    	<input type="text" placeholder="" name="horario" id="horarioinput">
      			  	</label>
      			</div>
      			<div class="medium-2 cell">
      			  	<label class="OpenSans-Regular col106a">Microfono
      			    	<input type="text" placeholder="" name="micro" id="microinput">
      			  	</label>
      			</div>
      			<div class="medium-2 cell">
      			  	<label class="OpenSans-Regular col106a">Edad
      			    	<input type="text" placeholder="" name="edad" id="edadinput">
      			  	</label>
      			</div>
      			<fieldset class="medium-10 cell text-center">
 				   <legend class="text-left fSize16 OpenSans-Regular col106a">Plataforma:</legend>
 				   	<input id="playstation" type="checkbox" name="plataforma" value="playstation"	>		<label for="playstation"> 	<i class="fab fa-playstation fSize18 col103a"></i> PlayStation</label>
 				   	<input id="xbox" 		type="checkbox" name="plataforma" value="xbox" 			>		<label for="xbox"> 		 	<i class="fab fa-xbox fSize18 col104a"></i> Xbox</label>
 				   	<input id="pc" 			type="checkbox" name="plataforma" value="pc" 			> 		<label for="pc"> 			<i class="fas fa-desktop fSize18"></i> PC</label>
 				   	<input id="mobile" 		type="checkbox" name="plataforma" value="mobile" 		>		<label for="mobile"> 		<i class="fas fa-mobile fSize18 "></i> Mobile</label>
 				 </fieldset>
 				 <fieldset class="medium-10 cell text-center ">
 				   <legend class="text-left fSize16 OpenSans-Regular col106a">Región:</legend>
 				   <input id="lan" 		type="checkbox" 	name="region" value="lan" 	> 		<label for="lan"> 	LAN 		</label>
 				   <input id="na" 		type="checkbox" 	name="region" value="na" 	> 		<label for="na"> 	NA 		 	</label>
 				   <input id="las" 		type="checkbox" 	name="region" value="las" 	> 		<label for="las"> 	LAS 	 	</label>
 				   <input id="eu" 		type="checkbox" 	name="region" value="eu" 	> 		<label for="eu"> 	EU 		 	</label>
 				   <input id="multi" 	type="checkbox" 	name="region" value="multi"	> 		<label for="multi"> Multiregión</label>
 				 </fieldset>
 				<div class="cell medium-5 text-center margin-top-3">
			 		<!--<a class="button success Fortnite-Font col100a fSize22 txtsh100a hvts101a tranText100a" href="">Continuar</a>-->
			 		<button type="submit" class="button">Enviar</button>
			 	</div>
			 	<div class="cell medium-5 text-center margin-top-3">
			 		<a class="button warning Fortnite-Font fSize22 col100a txtsh100a hvts101a tranText100a"  href="#">Cancelar</a>
			 	</div>
			 	<div class="cell HPX150">
			 	
			 	</div>
			 	</div>
			 	<input type="hidden" value="<?= base_url(); ?>" id="url">			  			
			</form>
		</div>
		<div class="cell large-4 bgg100a"><img src="<?php echo base_url(); ?>assets/img/llama.png"></div>
	</div>
</div>