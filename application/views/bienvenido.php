<body class=""> 
  <div class="grid-container full">  
  <div class="grid-x grid-padding-x  align-center">
    <div class="swiper-container swiperhome">     
      <div class="swiper-wrapper W100 HPX1000">
        <div class="swiper-slide cAnimation1a">
          <div class="cell large-12">
            <img src="<?php echo base_url(); ?>assets/img/swiper1.jpg" class="W100">
          </div>
        </div>
        <div class="swiper-slide cAnimation1a">
          <div class="cell large-12">
            <img src="<?php echo base_url(); ?>assets/img/swiper2.jpg" class="W100">
          </div>
        </div>
        <div class="swiper-slide cAnimation1a">
          <div class="cell large-12">
            <img src="<?php echo base_url(); ?>assets/img/swiper3.png" class="W100">
          </div>
        </div>
      </div>
    </div>
    <div class="cell large-8 position-absolute zIndex2 text-center MT150">
      <h2 class="Fortnite-Font fSize70 col102a txtsh100a">¡Unete a la mejor comunidad de clanes!</h2>
      <h2 class="Fortnite-Font fSize100 col100a txtsh100a">FORTNITE WORLD CLANS</h2>
      <div class="grid-x  padding-top-2 align-center">
        <div class="cell large-3">
          <a class="fSize30 Fortnite-Font col101a fSize50 txtsh100a hvts101a tranText100a" href="<?= base_url(); ?>index.php/FortniteClans/buscarClan">Buscar Clan</a>    
        </div>
        <div class="cell large-3">
          <a class="Fortnite-Font col101a fSize50 txtsh100a hvts101a tranText100a" href="<?= base_url(); ?>index.php/FortniteClans/crearClan">Crear clan</a>
        </div>
      </div>


    </div>
  </div>

</div>





