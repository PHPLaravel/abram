<body class="bgi100a">



<div class="grid-container">
	<div class="grid-x grid-margin-x HPX900">		
		<div class="cell large-8 bgs103a padding-top-3">
			<h1 class="Fortnite-Font fSize40 text-center col105a">Clan - <?= $clanes[0]->nombre ?> </h1>
			<?php foreach($clanes as $i){ ?>

				<div class="media-object stack-for-small">

					<div class="media-object-section">
						<div class="thumbnail">
						<?php if (count($i->path_file) == 0) { ?>

							<img src='<?= $i->file_name ?>'  width="300px" class="radius">

						<?php }else{ ?>

							<img src="<?= $i->path_file.'/'.$i->file_name ?>"  width="350px" class="radius">
							
							

						<?php	} ?>
						</div>
					</div>

					<div class="media-object-section">

						<p>
							&nbsp; <?= $i->nombre ?>		
						</p>	
						
						<p>
							<?php if($i->plataformas == "playstation") { ?>
							<label for="playstation"> 	
								&nbsp; <i class="fab fa-playstation fSize18 col103a"></i> 
								PlayStation
							</label>	
							<?php }else if($i->plataformas == "xbox" || $i->plataformas == "Xbox") { ?>
							<label for="xbox">
								&nbsp; <i class="fab fa-xbox fSize18 col104a"></i> 
								<?= $i->plataformas ?>
							</label>
							<?php }else if($i->plataformas == "pc"){ ?>
							<label for="pc">
								&nbsp; <i class="fas fa-desktop fSize18"></i> 
								<?= $i->plataformas ?>
							</label>
							<?php }else if($i->plataformas == "pc"){ ?>	
								<label for="mobile">
									&nbsp;	<i class="fas fa-mobile fSize18 "></i> 
									<?= $i->plataformas ?>
								</label>
							<?php } ?>	
						</p>
						<p>
							<label>
								&nbsp; <?= $i->wins ?>
							</label>
						</p>
						<p>
							<label>
								&nbsp; <?= $i->edad ?>
							</label>
						</p>
						<p>
							<label for="lan"> 	
								&nbsp; <?= $i->region ?> 		
							</label>
						</p>

	    				
					</div>
					
					

				</div>
			<?php	} ?>
		</div>
		<div class="cell large-4 bgg100a">
			<img src="<?php echo base_url(); ?>assets/img/llama.png">
		</div>
	</div>
</div>