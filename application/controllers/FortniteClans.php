<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FortniteClans extends CI_Controller {
	
  public function __construct()
    {
        parent::__construct();
       	$this->load->helper('url');
       	$this->load->model('ClansModel');
        $this->load->model('FilesModel');
        $this->load->model('UsersModel');
        $this->load->library("session");

    }
	 public function index()
    {
        $data['title'] = 'Fornite Clans';

        

        $this->load->view('layouts/header', $data);
        $this->load->view('bienvenido');
        $this->load->view('layouts/footer');
    }


    /* ------------ Create Clan ------------- */
    public function crearClan(){
    	$data['title'] = 'Crear Clan';
        $this->load->view('layouts/header', $data);
        $this->load->view('crear');
        $this->load->view('layouts/footer');
    }

    public function salvarClan(){

        //echo $this->input->post('nombre');
        $config['upload_path'] = './assets/img/clanes';
        $config['allowed_types'] = 'gif|jpg|png';

        

        $this->load->library('upload', $config);

        //$do = $this->upload->do_upload('profile_image');

  
        /*
        $data = $this->upload->data();

        echo "<pre>";
            var_dump($data);
        echo "</pre>";    
        */
        
        if (!$this->upload->do_upload('profile_image')) {

            $error = array('error' => $this->upload->display_errors());

            //$this->load->view('bienvenido', $error);

            //print_r($error);

            $nombre = "https://image.flaticon.com/icons/png/512/23/23716.png";

            $data = array(
                
                'file_name'=>$nombre,
                'nombre' => $this->input->post('nombre'),
                'plataformas' => $this->input->post('plataforma'),
                'wins' => $this->input->post('victorias'),
                'edad' => $this->input->post('edad'),
                'region'=>$this->input->post('region'),
                'id_user' => 1,
                'activo' => 'Si'
            );
           
             $this->ClansModel->insertarClan($data);
            
        } else {
            
            $data = array( $this->upload->data() );

            $name = $data[0]['file_name'];
            $filepath = $data[0]['file_path'];
            $fullpath = $data[0]['full_path'];
            $baseURL  = base_url('assets/img/clanes');
            $data = array(
                'path_file'=>$baseURL,
                'file_name'=>$name,
                'nombre' => $this->input->post('nombre'),
                'plataformas' => $this->input->post('plataforma'),
                'wins' => $this->input->post('victorias'),
                'edad' => $this->input->post('edad'),
                'region'=>$this->input->post('region'),
                'id_user' => 1,
                'activo' => 'Si'
            );
           
             $this->ClansModel->insertarClan($data);
        }

        

        
        /*
    	if($this->input->post()){
    		$nombre     =   $this->db->escape($nombre);
            $region     =   $this->db->escape($region);
            $plataforma =   $this->db->escape($plataforma);
    		$this->ClansModel->insertarClan($nombre, $region, $plataforma);
    	}
        */
    }
    /* ------------ Create Clan ------------- */

    /* ------------ View Clan ------------- */
    public function buscarClan(){

        $data['clanes'] = $this->ClansModel->buscarClan();

        $data['title'] = 'Fornite Clans';

        $this->load->view('layouts/header', $data);   
        $this->load->view('clanes/index',$data);
    }
    /* ------------ View Clan ------------- */

    /* ------------ Delete Clan ------------- */
    public function deleteClan($id){

        //echo $id;

        if( $this->ClansModel->deleteClan($id) ){

            $this->session->set_flashdata('correcto', 'Usuario eliminado correctamente');
            //redirect(base_url().'index.php/FortniteClans/');

        }else{

           $this->session->set_flashdata('incorrecto', 'Usuario eliminado correctamente');

           //redirect(base_url().'index.php/FortniteClans/');
        }
        //redirect(base_url().'');
        

    }

    public function login(){
        $this->load->view('login/login');
    }

    public function verifique(){

        $user = $this->input->post('user');
        $password = $this->input->post('password');

        $response['response'] = $this->UsersModel->login($user,$password);
        
        
        if( count($response) == 1 ){

            $data['title'] = 'Fornite Clans';
            //$response['response'] = $this->UsersModel->login($user,$password);

            $this->load->view('layouts/header', $data);
            $this->load->view('users/index.php',$response);
            $this->load->view('layouts/footer');


        }else{
            redirect( base_url().'index.php/FortniteClans/login' );
        }
        

    }



    public function editUser($id){

        $response['data'] = $this->UsersModel->buscarClan($id);
        $data['title'] = 'Fornite Clans';

        $this->load->view('layouts/header', $data);
        $this->load->view('users/edit',$response);
        $this->load->view('layouts/footer');

    }

    public function update($id){
        //echo $id;

        $data = array(
            'username' => $this->input->post('user'),
            'pass'     => $this->input->post('pass'),
        );

        $result = $this->UsersModel->findOrFaild($id,$data);

        if($result){

            $data['title'] = 'Fornite Clans';
            $data['mensaje'] = "Updated Success";
            $response['data'] = $this->UsersModel->buscarClan($id);

            $this->load->view('layouts/header', $data);
            $this->load->view('users/edit',$data,$response);
            $this->load->view('layouts/footer');
        }

    }

    public function showClan($id){
        //echo $id;
        $data['title'] = 'Fornite Clans';
        $data['clanes']= $this->ClansModel->showProfield($id);
        //var_dump($data);
        $this->load->view('layouts/header', $data); 
        $this->load->view('clanes/show',$data);
        $this->load->view('layouts/footer');
    }


}
